#include <stdlib.h>
#include <stdio.h>
int main() {
  void *a = malloc(32);
  void *b = malloc(64);
  char *c = calloc(5, sizeof (char));
  printf("c: '%s'\n", c);
  printf("a: '%p'\n", a);
  printf("b: '%p'\n", b);

  free(a);
  free(b);
  return 0;
}
