﻿CC=gcc
CFLAGS= -std=c99 -pedantic -Wall -Werror -fpic -Wextra
SRC= src/malloc.c src/malloc_function.c src/free_function.c
OBJ= $(SRC:.c=.o)
LIB= libmalloc.so

all: $(OBJ)
	$(CC)  -shared  $(CFLAGS)  $^ -o $(LIB)

clean:
	$(RM) src/malloc.o test libmalloc.so src/malloc_function.o src/free_function.o
	
check: tests/testsuite.c
	$(CC) $(CFLAGS)  -o test $^
