 #ifndef MALLOC_H
#define MALLOC_H

#define _DEFAULT_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>

#define FREE  0
#define NOT_FREE  1
#define PAGE_SIZE sysconf(_SC_PAGESIZE)


struct s_block
{
    void *next;
    void *next_free;
    void *last;
    int free; // free = 0 (free) free = 1 (occuped) 
    size_t size;
    char *data;
};

size_t word_align(size_t size);
int number_page(size_t size);
void *cast(void *ptr, size_t s);
void set_struct(struct s_block *current, struct s_block *previous, size_t s);
void *malloc_init(size_t s, struct s_block *last);
void *search_free_block(size_t size);
void *malloc(size_t size);
void free_fusion(struct s_block *block, struct s_block *next);
void free(void *ptr);
void *get_struct(void *ptr);
void *calloc(size_t number, size_t size);
void *realloc(void *ptr, size_t size);

#endif
