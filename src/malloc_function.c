#include "malloc.h"

void *get_struct(void *ptr) {
  char *pointer = ptr;
  pointer = pointer - sizeof(struct s_block) + sizeof(size_t);
  return pointer;
}

size_t word_align(size_t size) {
  size_t length = (size - 1) / sizeof(size_t);
  length *= sizeof(size_t);
  length += sizeof(size_t);
  return length;
}

int number_page(size_t size) {
  size_t length = size + sizeof(struct s_block);
  int rest = length % PAGE_SIZE;
  int nb_page = 0;
  if (rest == 0)
    nb_page = length / PAGE_SIZE;
  else
    nb_page = size / PAGE_SIZE + 1;
  return nb_page;
}

void set_struct(struct s_block *current, struct s_block *previous, size_t s)
{
  current->free = NOT_FREE;
  current->data = cast(current, sizeof (struct s_block));
  struct s_block *next_block = cast(current, s + sizeof (struct s_block));
  next_block->size = current->size - s - sizeof (struct s_block);
  next_block->free = FREE;
  next_block->next = current->next;
  next_block->last = current;
  next_block->next_free = current->next_free;

  current->size = s;
  current->next = next_block;
  current->next_free = next_block;

  while (previous != NULL)
  {
    struct s_block *tmp = previous->next_free;
    if (NULL != tmp && tmp->free == FREE)
      break;
    previous->next_free = next_block;
    previous = previous->last;
  }
}

void *malloc_init(size_t s, struct s_block *last)
{
  struct s_block *current = NULL;
  int nb_page = number_page(s);
  current = mmap(NULL, nb_page * PAGE_SIZE, PROT_READ | PROT_WRITE,
             MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
  if (current == MAP_FAILED)
  {
    errno = ENOMEM;
    return NULL;
  }
  current->free = NOT_FREE;
  current->data = cast(current, sizeof (struct s_block));
  current->size = s;
  current->last = last;
  struct s_block *next_block = cast(current, s + sizeof (struct s_block));
  next_block->size = nb_page * PAGE_SIZE - s - 2 * sizeof(struct s_block);
  next_block->free = FREE;
  next_block->last = current;
  next_block->next = NULL;
  next_block->next_free = NULL;
  current->next = next_block;
  current->next_free = next_block;

  return current;
}

