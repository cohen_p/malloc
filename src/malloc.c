#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <errno.h>
#include <memory.h>
#include "malloc.h"


void *g_pmap = NULL;

void *malloc(size_t size) {
  size_t s = word_align(size);
  struct s_block *init;
  if (g_pmap == NULL)
  {
    init = malloc_init(s, NULL);
    if (NULL == init)
      return NULL;
    g_pmap = init;
  }
  else
  {
    init = search_free_block(s);
    if (NULL == init)
      return NULL;
  }
  return init->data;
}

void *search_free_block(size_t size) {
  struct s_block *current = g_pmap;
  struct s_block *previous = NULL;
  size_t s = word_align(size);
  while (NULL != current->next_free)
  {
    if (current->free == FREE && current->size >= s + sizeof (struct s_block))
    {
      set_struct(current, previous, s);
      return current;
    }
    previous = current;
    current = current->next_free;
  }

  struct s_block *next_block = malloc_init(s, current);
  current->next_free = next_block->next_free;

  while (NULL != current->next)
    current = current->next;
  current->next = next_block;

  return current->next;
}

void free(void *ptr) {
  if (NULL == ptr)
    return;
  struct s_block *block = get_struct(ptr);
  if (block->data != ptr)
    return;
  block->free = FREE;

  struct s_block *next_block = block->next;

  if (next_block->free == FREE)
   free_fusion(block, next_block);
}

void *calloc(size_t number, size_t size) {
  size_t length = number * size;
  char *ptr = malloc(length);
  if (ptr == NULL)
    return NULL;
  char *dst = ptr;
  for (size_t i = 0; i < length; *dst = 0, ++dst, ++i)
    ptr[i] = 0;
  return ptr;
}

void *realloc(void *ptr, size_t size) {
  struct s_block *block;
  if (NULL == ptr)
    return malloc(size);
  if (NULL != ptr && size == 0)
    free(ptr);
  block = get_struct(ptr);
  if (size <= block->size)
  {
     block->size = size;
     return ptr;
  }
  struct s_block *new_block = search_free_block(size);
  memcpy(new_block, ptr, block->size);
  free(ptr);
  return new_block;
}
