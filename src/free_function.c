#include "malloc.h"

void *cast(void *ptr, size_t s)
{
  char *data = ptr;
  data += s;
  void *pv_data = data;
  return pv_data;
}

void free_fusion(struct s_block *block, struct s_block *next_block)
{
  block->size = block->size + next_block->size + sizeof (struct s_block);
  block->next = next_block->next;
  block->next_free = next_block->next_free;
}

